////////////////////////////////////////
// DISPLAY.cpp
////////////////////////////////////////

#include "display.h"

#define WINDOWTITLE	"PA1"

using namespace std;

void Display::InitializeWorld()
{
	// Build the broadphase
	btBroadphaseInterface* broadphase = new btDbvtBroadphase();

	// Set up the collision configuration and dispatcher
	btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();
	btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfiguration);

	// The actual physics solver
	btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver;

	// The world.
	dynamicsWorld = new btSoftRigidDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
	dynamicsWorld->setGravity(btVector3(0, -9.81f, 0));

	dynamicsWorld->setDebugDrawer(&dd);
	dd.setDebugMode(btIDebugDraw::DBG_DrawWireframe);
}
////////////////////////////////////////////////////////////////////////////////

void Display::InitializeObjects(btSoftBodyWorldInfo& info)
{
	Vector3 v3(-5, 0, 5);
	int name = 1;
	Shape *sphere = new Sphere();
	name++;
	Shape *plane = new Plane(v3);
	name++;
	Objects.push_back(sphere);
	Objects.push_back(plane);

	float x = -5, y = -1, z = -10;
	float zRate = 0.5;
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j <= 10; j++)
		{
			Shape* cube = new Cube(x, y, z);
			Objects.push_back(cube);
			z += 2;
			name++;
		}
		zRate *= -1;
		z = -10 + zRate ;
		y += 2.1;
	}

	Shape* rope = new Rope(0, 0, 10, 0, 0, 0, info);
	Objects.push_back(rope);

}

////////////////////////////////////////////////////////////////////////////////

void Display::AddRigidBody()
{
	Shape* ball = new Sphere();
	for (list<Shape*>::const_iterator iterator = Objects.begin(), end = Objects.end(); iterator != end; ++iterator) {
		if ((*iterator)->GetShape().compare("rope") != 0)
		{
			dynamicsWorld->addRigidBody((*iterator)->GetRegidBody());
			if ((*iterator)->GetShape().compare("sphere") == 0)
			{
				delete ball;
				ball = (*iterator);
			}
		}
		else
		{
			dynamicsWorld->addSoftBody((*iterator)->GetSoftBody());
			(*iterator)->GetSoftBody()->appendAnchor(11, ball->GetRegidBody(), false);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

Display::Display() {
	WinX = 640;
	WinY = 480;
	LeftDown = MiddleDown = RightDown = false;
	MouseX = MouseY = 0;

	// Create the window
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(WinX, WinY);
	glutInitWindowPosition(0, 0);
	WindowHandle = glutCreateWindow(WINDOWTITLE);
	glutSetWindowTitle(WINDOWTITLE);
	glutSetWindow(WindowHandle);

	glClearDepth(1.0f);
	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);

	//glEnable(GL_CULL_FACE);
	//glEnable(GL_LIGHTING);
	//glEnable(GL_LIGHT0);

	//GLfloat lightpos[] = { 20, 10, -20, 0. };
	//glLightfv(GL_LIGHT0, GL_POSITION, lightpos);


	// Background color
	glClearColor(0.3, 0.3, 0.3, 1.0);

	// Initialize components
	Cam.SetAspect(float(WinX) / float(WinY));
	InitializeWorld();
	InitializeObjects(dynamicsWorld->getWorldInfo());
	AddRigidBody();
}

////////////////////////////////////////////////////////////////////////////////

Display::~Display() {
	glFinish();
	glutDestroyWindow(WindowHandle);
}

////////////////////////////////////////////////////////////////////////////////

void Display::Update() {
	//update the dynamics world
	dynamicsWorld->stepSimulation(1 / 30.f, 10);

	// Update the components in the world
	Cam.Update();

	for (list<Shape*>::const_iterator iterator = Objects.begin(), end = Objects.end(); iterator != end; ++iterator) {
		(*iterator)->Update();
	}

	computeFPS();

	// Tell glut to re-display the scene
	glutSetWindow(WindowHandle);
	glutPostRedisplay();
}

////////////////////////////////////////////////////////////////////////////////

void Display::Reset() {
	Cam.Reset();
	Cam.SetAspect(float(WinX) / float(WinY));

	for (list<Shape*>::const_iterator iterator = Objects.begin(), end = Objects.end(); iterator != end; ++iterator) {
		(*iterator)->Reset();
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::Draw() {

	if (!wireframe)
	{
		glViewport(0, 0, WinX, WinY);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		dynamicsWorld->debugDrawWorld();
	}
	else
	{
		if (usedDraw2)
		{
			clickedObject->Draw();
			usedDraw2 = false;
		}
		else
		{
			// Begin drawing scene
			glViewport(0, 0, WinX, WinY);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glMatrixMode(GL_MODELVIEW);

			for (list<Shape*>::const_iterator iterator = Objects.begin(), end = Objects.end(); iterator != end; ++iterator)
			{
				(*iterator)->Draw();
			}
		}
	}

	// Finish drawing scene
	glFinish();
	glutSwapBuffers();
}

void Display::Draw2() {

	// Begin drawing scene
	glViewport(0, 0, WinX, WinY);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	for (list<Shape*>::const_iterator iterator = Objects.begin(), end = Objects.end(); iterator != end; ++iterator) {
		if (!(*iterator)->picked)
			(*iterator)->Draw();
	}

	usedDraw2 = true;
}

////////////////////////////////////////////////////////////////////////////////

void Display::Quit() {
	glFinish();
	glutDestroyWindow(WindowHandle);

for (list<Shape*>::const_iterator iterator = Objects.begin(), end = Objects.end(); iterator != end; ++iterator) {
	delete (*iterator);
}
exit(0);
}

////////////////////////////////////////////////////////////////////////////////

void Display::Resize(int x, int y) {
	WinX = x;
	WinY = y;
	Cam.SetAspect(float(WinX) / float(WinY));
}

////////////////////////////////////////////////////////////////////////////////

void Display::Keyboard(int key, int x, int y) {
	const float rate = 1.0f;

	switch (key) {
	case 0x1b:		// Escape
		Quit();
		break;
	case 'r':
		Reset();
		break;
	case 'w':
		Cam.SetIncline(Cam.GetIncline() + rate);
		break;
	case 's':
		Cam.SetIncline(Cam.GetIncline() - rate);
		break;
	case 'a':
		Cam.SetAzimuth(Cam.GetAzimuth() + rate);
		break;
	case 'd':
		Cam.SetAzimuth(Cam.GetAzimuth() - rate);
		break;
	case 'v':
		wireframe = !wireframe;
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::MouseButton(int btn, int state, int x, int y) {
	if (btn == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN)
		{
			LeftDown = true;
			if (!objectClicked)
			{
				MouseInWorld = GetOGLPos(x, y);
				ObjectPicking();
			}
		}
		else
		{
			if (objectClicked)
			{
				clickedObject->GetRegidBody()->activate(true);
				clickedObject->picked = false;
			}
			objectClicked = false;
			LeftDown = false;
		}
	}
	else if (btn == GLUT_MIDDLE_BUTTON) {
		MiddleDown = (state == GLUT_DOWN);
	}
	else if (btn == GLUT_RIGHT_BUTTON) {
		RightDown = (state == GLUT_DOWN);
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::MouseMotion(int nx, int ny) {
	int dx = nx - MouseX;
	int dy = -(ny - MouseY);

	MouseX = nx;
	MouseY = ny;

	// Move camera
	// NOTE: this should really be part of Camera::Update()
	if (RightDown) {
		const float rate = 1.0f;
		Cam.SetAzimuth(Cam.GetAzimuth() + dx*rate);
		Cam.SetIncline(Cam.GetIncline() - dy*rate);
	}
	if (MiddleDown) {
		const float rate = 0.01f;
		Cam.SetDistance(Cam.GetDistance()*(1.0f - dx*rate));
	}
	if (LeftDown)
	{
		if (objectClicked)
		{
			if (!usedDraw2) Draw2();
			MouseInWorld = GetOGLPos(MouseX, MouseY);
			clickedObject->Move(MouseInWorld.x, MouseInWorld.y + 1, MouseInWorld.z);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::ObjectPicking() {
	for (list<Shape*>::const_iterator iterator = Objects.begin(), end = Objects.end(); iterator != end; ++iterator) {
		if ( abs((*iterator)->xLoc - MouseInWorld.x) < 2.0f && ((abs((*iterator)->yLoc - MouseInWorld.y)) < 2.0f)
			&& ((abs((*iterator)->zLoc - MouseInWorld.z)) < 2.0f))
			{
				objectClicked = true;
				clickedObject = (*iterator);
				clickedObject->picked = true;
				break;
			}
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::computeFPS()
{
	// post: compute frames per second and display in window's title bar
	frameCount++;
	int currentTime = glutGet(GLenum(GLUT_ELAPSED_TIME));
	if (currentTime - lastFrameTime > 1000)
	{
		char s[16];
		sprintf(s, "FPS: %4.2f",
			frameCount * 1000.0 / (currentTime - lastFrameTime));
		glutSetWindowTitle(s);

		lastFrameTime = currentTime;
		frameCount = 0;
	}
}
