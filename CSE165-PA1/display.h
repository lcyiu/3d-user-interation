////////////////////////////////////////
// display.h
////////////////////////////////////////

#ifndef CSE169_TESTER_H
#define CSE169_TESTER_H

#include <list>
#include "core.h"
#include "camera.h"
#include "cube.h"
#include "sphere.h"
#include "shape.h"
#include "plane.h"
#include "rope.h"
#include <btBulletDynamicsCommon.h>
#include "BulletSoftBody\btSoftRigidDynamicsWorld.h"

#include "debugDraw.h"

////////////////////////////////////////////////////////////////////////////////

class Display {
public:
	Display();
	~Display();

	void Update();
	void Reset();
	void Draw();
	void Draw2();

	void Quit();

	// Event handlers
	void Resize(int x,int y);
	void Keyboard(int key,int x,int y);
	void MouseButton(int btn,int state,int x,int y);
	void MouseMotion(int x,int y);
	void InitializeObjects(btSoftBodyWorldInfo &);
	void ObjectPicking();
	void computeFPS();

	// Components
	Camera Cam;
	//objects
	std::list<Shape*> Objects;
	DebugDraw dd;

private:

	void InitializeWorld();
	void AddRigidBody();

	// Window management
	int WindowHandle;
	int WinX,WinY;

	// Input
	bool LeftDown,MiddleDown,RightDown;
	int MouseX,MouseY;
	Vector3 MouseInWorld;

	//variables
	bool objectClicked = false;
	Shape* clickedObject;
	bool usedDraw2 = false;
	int frameCount = 0;
	int lastFrameTime = 0;
	bool wireframe = false;

	btSoftRigidDynamicsWorld* dynamicsWorld;
};

////////////////////////////////////////////////////////////////////////////////

//Code Source
//https://www.opengl.org/discussion_boards/showthread.php/175823-From-Screen-Coordinate-to-OpenGL-coordinate
static Vector3 GetOGLPos(int x, int y)
{
	GLint viewport[4];
	GLdouble modelview[16];
	GLdouble projection[16];
	GLfloat winX, winY, winZ;
	GLdouble posX, posY, posZ;

	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	glGetDoublev(GL_PROJECTION_MATRIX, projection);
	glGetIntegerv(GL_VIEWPORT, viewport);

	winX = (float)x;
	winY = (float)viewport[3] - (float)y;
	glReadPixels(x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

	gluUnProject(winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);

	return Vector3(posX, posY, posZ);
};

////////////////////////////////////////////////////////////////////////////////

#endif
