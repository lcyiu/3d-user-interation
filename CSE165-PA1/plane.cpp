////////////////////////////////////////
// plane.cpp
////////////////////////////////////////

#include "plane.h"

////////////////////////////////////////////////////////////////////////////////

Plane::Plane(Vector3 tl) {
	// Initialize constant data
	Size = 1.0f;
	Position = Vector3::ORIGIN;
	Axis = Vector3::YAXIS;
	topLeft = tl;
	topRight = tl;
	bottomLeft = tl;
	bottomRight = tl;

	collisionShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
	motionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, -3, 0)));
	btRigidBody::btRigidBodyConstructionInfo
		groundRigidBodyCI(0, motionState, collisionShape, btVector3(0, 0, 0));
	rigidBody = new btRigidBody(groundRigidBodyCI);

	// Resets variable data
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

Plane::~Plane() {
	delete collisionShape;
	delete motionState;
	delete rigidBody;
}

////////////////////////////////////////////////////////////////////////////////

void Plane::Update()
{
	// Update (animate) any variable properties
}

////////////////////////////////////////////////////////////////////////////////

void Plane::Reset()
{
	// Reset dynamic variables to a default state
	Angle = 0.0f;
	WorldMtx.Identity();
}

////////////////////////////////////////////////////////////////////////////////

void Plane::Draw()
{
	glPushMatrix();

	GLfloat red[] = { 0.5f, 0.5f, 0.5f, 1.f };
	glColor3f(1, 1, 1);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, red);

	glLoadMatrixf(WorldMtx);
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-20, -2, 20);
	glVertex3f(20, -2, 20);
	glVertex3f(20, -2, -20);
	glVertex3f(-20, -2, -20);
	glEnd();
	glPopMatrix();
}

////////////////////////////////////////////////////////////////////////////////

void Plane::Move(float x, float y, float z)
{

}

////////////////////////////////////////////////////////////////////////////////


