////////////////////////////////////////
// cube.cpp
////////////////////////////////////////

#include "cube.h"

////////////////////////////////////////////////////////////////////////////////

Cube::Cube(float x, float y, float z) {
	// Initialize constant data
	Size=2.0f;
	Position=Vector3::ORIGIN;
	Axis=Vector3::YAXIS;
	xLoc = x;
	yLoc = y;
	zLoc = z;

	collisionShape = new btBoxShape(btVector3(1,1,1));
	motionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(xLoc, yLoc, zLoc)));
	btScalar mass = 2;
	btVector3 fallInertia(0, 0, 0);
	collisionShape->calculateLocalInertia(mass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, motionState, collisionShape, fallInertia);
	rigidBody = new btRigidBody(fallRigidBodyCI);
	rigidBody->activate(false);
	picked = false;

	// Resets variable data
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

Cube::~Cube() {
	delete collisionShape;
	delete motionState;
	delete rigidBody;
}

////////////////////////////////////////////////////////////////////////////////

void Cube::Update()
{
	// Update (animate) any variable properties
	if (picked)
	{
		Move(xLoc, yLoc, zLoc);
	}
	else
	{
		btTransform trans;
		rigidBody->getMotionState()->getWorldTransform(trans);

		xLoc = trans.getOrigin().getX();
		yLoc = trans.getOrigin().getY();
		zLoc = trans.getOrigin().getZ();

		btScalar m[16];
		trans.getOpenGLMatrix(m);

		WorldMtx = Matrix34(m[0], m[4], m[8], m[12], m[1], m[5], m[9], m[13], m[2], m[6], m[10], m[14]);
	}
}

////////////////////////////////////////////////////////////////////////////////

void Cube::Reset()
{
}

////////////////////////////////////////////////////////////////////////////////

void Cube::Draw()
{
	glPushMatrix();

	GLfloat blue[] = { 0.f, .0f, 1.0f, 1.f };
	glMaterialfv(GL_FRONT, GL_DIFFUSE, blue);

	glColor3f(0, 0, 1);
	glLoadMatrixf(WorldMtx);
	if (picked)
	{
		glLoadIdentity(); 
		glTranslatef(xLoc, yLoc, zLoc);
	}
	glutSolidCube(Size);

	GLfloat white[] = { 1.f, 1.0f, 1.0f, 1.f };
	glColor3f(1, 1, 1);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, white);
	glutWireCube(Size);
	glPopMatrix();
}

////////////////////////////////////////////////////////////////////////////////

void Cube::Move(float x, float y, float z)
{
	y = y < -1 ? -1 : y;

	// Reset dynamic variables to a default state
	motionState->setWorldTransform(btTransform(btQuaternion(0, 0, 0, 1), btVector3(x, y, z)));
	rigidBody->setMotionState(motionState);
	rigidBody->setLinearVelocity(btVector3(0, 0, 0));
	xLoc = x;
	yLoc = y;
	zLoc = z;
}

////////////////////////////////////////////////////////////////////////////////



