////////////////////////////////////////
// rope.h
////////////////////////////////////////

#ifndef CSE169_ROPE_H
#define CSE169_ROPE_H

#include "core.h"
#include "vector3.h"
#include "matrix34.h"
#include "shape.h"
#include "BulletSoftBody\btSoftBody.h"
#include "BulletSoftBody\btSoftBodyHelpers.h"

#define RES 10

////////////////////////////////////////////////////////////////////////////////

class Rope : public Shape
{
public:
	Rope(float x1, float x2, float y1, float y2, float z1, float z2, btSoftBodyWorldInfo& );
	~Rope();
	
	void Update();
	void Reset();
	void Draw();
	void Move(float x, float y, float z);
	string GetShape() { return "rope"; };

	float x1;
	float x2;
	float y1;
	float y2;
	float z1;
	float z2;
};

////////////////////////////////////////////////////////////////////////////////

#endif
