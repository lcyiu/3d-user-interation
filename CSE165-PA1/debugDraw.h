#ifndef DD_H
#define DD_H

#include "LinearMath\btIDebugDraw.h"
#include "core.h"

class DebugDraw : public btIDebugDraw
{
public:
	DebugDraw(){};

	void drawLine(const btVector3& from, const btVector3& to, const btVector3& color)
	{
		glColor3f(color[0], color[1], color[2]);
		glBegin(GL_LINES);
		glVertex3f(from[0], from[1], from[2]);
		glVertex3f(to[0], to[1], to[2]);
		glEnd();
	}

	void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color)
	{
	};

	void reportErrorWarning(const char* warningString)
	{
	};

	void draw3dText(const btVector3& location, const char* textString)
	{
	};

	void setDebugMode(int debugMode) 
	{
		dm = debugMode;
	};

	int	getDebugMode() const
	{
		return dm;
	};

private:
	int dm = DBG_NoDebug;
};

#endif