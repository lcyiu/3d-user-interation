////////////////////////////////////////
// plane.h
////////////////////////////////////////

#ifndef CSE169_PLANE_H
#define CSE169_PLANE_H

#include "core.h"
#include "vector3.h"
#include "matrix34.h"
#include "shape.h"

////////////////////////////////////////////////////////////////////////////////

class Plane : public Shape
{
public:
	Plane(Vector3 tl);
	~Plane();

	void Update();
	void Reset();
	void Draw();
	void Move(float x, float y, float z);
	string GetShape() { return "plane"; };

private:
	Vector3 topLeft ;
	Vector3 topRight;
	Vector3 bottomLeft;
	Vector3 bottomRight;
};

////////////////////////////////////////////////////////////////////////////////

#endif
