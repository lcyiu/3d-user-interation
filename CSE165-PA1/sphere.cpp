////////////////////////////////////////
// sphere.cpp
////////////////////////////////////////

#include "sphere.h"
#include <iostream>

////////////////////////////////////////////////////////////////////////////////

Sphere::Sphere() {
	// Initialize constant data
	Size = 1.0f;
	Position = Vector3::ORIGIN;
	Axis = Vector3::YAXIS;

	xLoc = DEFAULTX;
	yLoc = DEFAULTY;
	zLoc = DEFAULTZ;

	collisionShape = new btSphereShape(Size);
	motionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(DEFAULTX, DEFAULTY, DEFAULTZ)));
	btScalar mass = MASS;
	btVector3 fallInertia(0, 0, 0);
	collisionShape->calculateLocalInertia(mass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, motionState, collisionShape, fallInertia);
	rigidBody = new btRigidBody(fallRigidBodyCI);
	rigidBody->forceActivationState(DISABLE_DEACTIVATION);
	picked = false;

	// Resets variable data
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

Sphere::~Sphere() {
	delete collisionShape;
	delete motionState;
	delete rigidBody;
}

////////////////////////////////////////////////////////////////////////////////

void Sphere::Update()
{
	if (picked)
	{
		Move(xLoc, yLoc, zLoc);
	}
	else
	{
		// Update (animate) any variable properties
		btTransform trans;
		rigidBody->getMotionState()->getWorldTransform(trans);

		xLoc = trans.getOrigin().getX();
		yLoc = trans.getOrigin().getY();
		zLoc = trans.getOrigin().getZ();

	}

}

////////////////////////////////////////////////////////////////////////////////

void Sphere::Reset()
{
	// Reset dynamic variables to a default state
	motionState->setWorldTransform(btTransform(btQuaternion(0, 0, 0, 1), btVector3(DEFAULTX, DEFAULTY, DEFAULTZ)));
	rigidBody->setMotionState(motionState);

	Angle = 0.0f;
	WorldMtx.Identity();
}

////////////////////////////////////////////////////////////////////////////////

void Sphere::Draw()
{
	glPushMatrix();
	GLfloat white[] = { 0.2f, 0.2f, 0.2f, 1.f };
	glMaterialfv(GL_FRONT, GL_DIFFUSE, white);
	glLoadMatrixf(WorldMtx);
	glColor3f(0.1f, 0.1f, 0.1f);
	glTranslatef(xLoc, yLoc, zLoc);
	glutSolidSphere(Size, 10, 10);
	glPopMatrix();
}

////////////////////////////////////////////////////////////////////////////////

void Sphere::Move(float x, float y, float z)
{
	if (y != yLoc || x != xLoc || z != zLoc)
	{
		if (x - xLoc <= 0)
		{
			yLoc += xLoc < 0 ? 0.1 : -0.1;
		}
		else
		{
			yLoc += xLoc > 0 ? 0.1 : -0.1;
		}
			
	}

	x = x > 10 ? 10 : x; x = x < -10 ? -10 : x;
	yLoc = yLoc > 10 ? 10 : yLoc; yLoc = yLoc < -2 ? -2 : yLoc;
	z = z > 10 ? 10 : z; z = z < -10 ? -10 : z;

	// Reset dynamic variables to a default state
	motionState->setWorldTransform(btTransform(btQuaternion(0, 0, 0, 1), btVector3(x, yLoc, z)));
	rigidBody->setMotionState(motionState);
	rigidBody->setLinearVelocity(btVector3(0, 0, 0));
	rigidBody->activate(false);
	xLoc = x;
	zLoc = z;
}

////////////////////////////////////////////////////////////////////////////////


