////////////////////////////////////////
//Code Source
//CSE169 Starter Code
// camera.cpp
////////////////////////////////////////

#include "camera.h"

////////////////////////////////////////////////////////////////////////////////

Camera::Camera() {
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

void Camera::Update() {
	if (moveX < 0)
	{
		xloc -= 0.1;
	}
	if (moveX > 0)
	{
		xloc += 0.1;
	}
	if (moveY < 0)
	{
		yloc += 0.1;
	}
	if (moveY > 0)
	{
		yloc -= 0.1;
	}
}

////////////////////////////////////////////////////////////////////////////////

void Camera::Reset() {
	FOV=60.0f;
	Aspect=1.33f;
	NearClip=0.1f;
	FarClip=200.0f;

	Distance=30.0f;
	Azimuth=0.0f;
	Incline=0.0f;
	xloc = DEFAULTX;
	yloc = DEFAULTY;
}

////////////////////////////////////////////////////////////////////////////////

void Camera::Draw() {
	// Tell GL we are going to adjust the projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Set perspective projection
	gluPerspective(FOV,Aspect,NearClip,FarClip);

	// Place camera
	glRotatef(Incline, 1.0f, 0.0f, 0.0f);
	glRotatef(Azimuth, 0.0f, 1.0f, 0.0f);
	glTranslatef(xloc, yloc, -Distance);

	// Return to modelview matrix mode
	glMatrixMode(GL_MODELVIEW);
}

////////////////////////////////////////////////////////////////////////////////
