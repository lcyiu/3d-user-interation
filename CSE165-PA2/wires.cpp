////////////////////////////////////////
// wire.cpp
////////////////////////////////////////

#include "wires.h"
#include <windows.h>		// Header File For Windows
#include <iostream>

////////////////////////////////////////////////////////////////////////////////

Wires::Wires() {
	// Initialize constant data
	Size = 1.0f;
	Position = Vector3::ORIGIN;
	Axis = Vector3::YAXIS;

	xLoc = 0;
	yLoc = 0;
	zLoc = 0;

	// Resets variable data
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

Wires::~Wires() {
	delete collisionShape;
	delete motionState;
	delete rigidBody;
	for (std::list<Wire*>::iterator it = wires.begin(); it != wires.end(); ++it)
	{
		delete (*it);
	}
}

////////////////////////////////////////////////////////////////////////////////

void Wires::Update()
{
	//if (picked)
	//{
	//	Move(xLoc, yLoc, zLoc);
	//}
	//else
	//{
	//	// Update (animate) any variable properties
	//	btTransform trans;
	//	rigidBody->getMotionState()->getWorldTransform(trans);

	//	xLoc = trans.getOrigin().getX();
	//	yLoc = trans.getOrigin().getY();
	//	zLoc = trans.getOrigin().getZ();
	//}

	for (std::list<Wire*>::iterator it = wires.begin(); it != wires.end(); ++it)
	{
		(*it)->Update();
	}
}

////////////////////////////////////////////////////////////////////////////////

void Wires::Reset()
{
	for (std::list<Wire*>::iterator it = wires.begin(); it != wires.end(); ++it)
	{
		delete (*it);
	}
	wires.clear();
}

////////////////////////////////////////////////////////////////////////////////

void Wires::Draw()
{
	for (std::list<Wire*>::iterator it = wires.begin(); it != wires.end(); ++it)
	{
		(*it)->Draw();
	}
}

////////////////////////////////////////////////////////////////////////////////

void Wires::Move(float x, float y, float z)
{
	lastX = xLoc;
	lastY = yLoc;
	lastZ = zLoc;

	xLoc = x;
	yLoc = y;
	zLoc = z;
}

////////////////////////////////////////////////////////////////////////////////

void Wires::AddWire(Wire* w)
{
	wires.push_back(w);
}

////////////////////////////////////////////////////////////////////////////////

Wire* Wires::SearchWire(Vector3 v)
{
	for (std::list<Wire*>::iterator it = wires.begin(); it != wires.end(); ++it)
	{
		Vector3 loc = (*it)->to;
		if ((v - loc).Mag() < 1.5)
		{
			return (*it);
		}
	}
	return NULL;
}



