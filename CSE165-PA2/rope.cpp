////////////////////////////////////////
// rope.cpp
////////////////////////////////////////

#include "rope.h"

////////////////////////////////////////////////////////////////////////////////

Rope::Rope(float x1, float x2, float y1, float y2, float z1, float z2, btSoftBodyWorldInfo& info) {
	// Initialize constant data
	Size = 1.0f;
	Position = Vector3::ORIGIN;
	Axis = Vector3::YAXIS;

	this->x1 = x1;
	this->x2 = x2;
	this->y1 = y1;
	this->y2 = y2;
	this->z1 = z1;
	this->z2 = z2;

	softBody = btSoftBodyHelpers::CreateRope(info, btVector3(x1,y1,z1), btVector3(x2,y2,z2), RES , 1);

	// Resets variable data
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

Rope::~Rope() {
	delete softBody;
}

////////////////////////////////////////////////////////////////////////////////

void Rope::Update()
{
}

////////////////////////////////////////////////////////////////////////////////

void Rope::Reset()
{
	// Reset dynamic variables to a default state
	Angle = 0.0f;
	WorldMtx.Identity();
}

////////////////////////////////////////////////////////////////////////////////

void Rope::Draw()
{

	glPushMatrix();
	GLfloat white[] = { 1.f, 1.0f, 1.0f, 1.f };
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, white);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

	glLoadMatrixf(WorldMtx);
	glLineWidth(2.5);
	glBegin(GL_LINES);

	btSoftBody::tNodeArray nodes = softBody->m_nodes;
	for (int i = 0; i < RES; i++)
	{
		btVector3 from = nodes[i].m_x;
		btVector3 to = nodes[i+1].m_x;
		glVertex3f(from.getX(), from.getY(), from.getZ());
		glVertex3f(to.getX(), to.getY(), to.getZ());
	}

	glEnd();
	glPopMatrix();
}

////////////////////////////////////////////////////////////////////////////////

void Rope::Move(float x, float y, float z)
{

}

////////////////////////////////////////////////////////////////////////////////


