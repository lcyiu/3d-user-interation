#ifndef CSE169_HAND_H
#define CSE169_HAND_H

#include "core.h"

////////////////////////////////////////////////////////////////////////////////

class MyHand {
public:
	MyHand(){ active = false; };
	void setPosition(float x1, float y1, float z1){ x = x1, y = y1, z = z1; };
	void movePosition(float dx, float dy, float dz){ x += dx, y += dy, z += dz; };
	bool isActive(){ return active; };
	void setActive(bool a) { active = a; };
	void setPalmZero() { pitch = 0.0; yaw = 0.0; };
	void setAngle(float p, float y) { pitch = p; yaw = y; };

public:
	float x, y, z;
	float pitch, yaw;
	bool active;
	bool undo = false;
	bool moveBall = false;
	bool clap = false;
};

#endif
