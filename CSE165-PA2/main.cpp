////////////////////////////////////////
// DISPLAY.cpp
////////////////////////////////////////

#include "display.h"

#define WINDOWTITLE	"PA1"

using namespace std;

////////////////////////////////////////////////////////////////////////////////

static Display *DISPLAY;

////////////////////////////////////////////////////////////////////////////////

static void timer(int)
{
	DISPLAY->Update();
	glutPostRedisplay();

	glutTimerFunc(1000/120, timer, 1);
}

// These are really HACKS to make glut call member functions instead of static functions
static void display()									{ DISPLAY->Cam.Draw();  DISPLAY->Draw(); }
static void idle()										{ DISPLAY->Update(); }
static void resize(int x, int y)							{ DISPLAY->Resize(x, y); }
static void keyboard(unsigned char key, int x, int y)		{ DISPLAY->Keyboard(key, x, y); }
static void keyboardUp(unsigned char key, int x, int y)		{ DISPLAY->KeyboardUp(key, x, y); }
static void mousebutton(int btn, int state, int x, int y)	{ DISPLAY->MouseButton(btn, state, x, y); }
static void mousemotion(int x, int y)					{ DISPLAY->MouseMotion(x, y); }

static void glutFunctioinCalls() 
{
	glutDisplayFunc(display);
	//glutIdleFunc(idle);
	glutTimerFunc(1000 / 60, timer, 1);
	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyboardUp);
	glutMouseFunc(mousebutton);
	glutMotionFunc(mousemotion);
	glutPassiveMotionFunc(mousemotion);
	glutReshapeFunc(resize);
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	DISPLAY = new Display();
	glutFunctioinCalls();
	glutMainLoop();

	return 0;
}

////////////////////////////////////////////////////////////////////////////////




