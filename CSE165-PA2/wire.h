////////////////////////////////////////
// wire.h
////////////////////////////////////////

#ifndef CSE169_WIRE_H
#define CSE169_WIRE_H

#include "core.h"
#include "vector3.h"
#include "matrix34.h"
#include "shape.h"
#include <list>

#define MASS 10
#define RADIUS 0.5

////////////////////////////////////////////////////////////////////////////////

class Wire : public Shape
{
public:
	Wire();
	Wire(Vector3 from, Vector3 to, GLUquadricObj* obj, float h);
	~Wire();

	void Update();
	void Reset();
	void Draw();
	void Move(float x, float y, float z);
	string GetShape() { return "wire"; };
	Vector3 GetPosition() { return Vector3(xLoc, yLoc, zLoc); };
	void GetRotationMatrix(float x1, float y1, float z1, float x2, float y2, float z2);

	float height;
	GLUquadricObj* obj;
	float angleXY;
	float angleXZ;

	Vector3 from;
	Vector3 to;
	bool selected = false;
	GLfloat model[16];
	GLdouble model2[16];
	btFixedConstraint* fixedConstraint;
};

////////////////////////////////////////////////////////////////////////////////

#endif
