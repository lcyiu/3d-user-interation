////////////////////////////////////////
//Code Source
//CSE169 Starter Code
// core.h
////////////////////////////////////////

#ifndef CSE169_CORE_H
#define CSE169_CORE_H

////////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#define M_PI	3.14159f
#include <windows.h>
#endif

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <ctype.h>
#include <iostream>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <btBulletDynamicsCommon.h>

#define PLANESIZE 40

using namespace std;

void drawAxis(float size);
void drawWireBox(float xmin,float ymin,float zmin,float xmax,float ymax,float zmax);

////////////////////////////////////////////////////////////////////////////////

/*
This file just has a bunch of common stuff used by all objects. It mainly just
includes GL and some other standard headers.
*/

//Source Code
//http://stackoverflow.com/questions/9430852/glutbitmapcharacter-positions-text-wrong
static void printLocation(float x, float y, float z, int mode)
{
	// TEXT
	glMatrixMode(GL_PROJECTION);
	glPushMatrix(); // save
	glLoadIdentity();// and clear
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST); // also disable the depth test so renders on top

	glColor4f(1.0f, 0, 0, 1.0f);

	glRasterPos2f(-1, .95); // center of screen. (-1,0) is center left.
	char buf[300];
	sprintf(buf, "x : %f", x);
	const char * p = buf;
	do glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p); while (*(++p));

	sprintf(buf, "y : %f", y);
	p = buf;
	glRasterPos2f(-1, .9); // center of screen. (-1,0) is center left.
	do glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p); while (*(++p));

	sprintf(buf, "z : %f", z);
	p = buf;
	glRasterPos2f(-1, .85); // center of screen. (-1,0) is center left.
	do glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p); while (*(++p));

	sprintf(buf, "mode : %d", mode);
	p = buf;
	glRasterPos2f(-1, .80); // center of screen. (-1,0) is center left.
	do glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p); while (*(++p));

	glEnable(GL_DEPTH_TEST); // Turn depth testing back on

	glMatrixMode(GL_PROJECTION);
	glPopMatrix(); // revert back to the matrix I had before.
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}



#endif
