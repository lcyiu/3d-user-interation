////////////////////////////////////////
// display.h
////////////////////////////////////////

#ifndef CSE169_TESTER_H
#define CSE169_TESTER_H

#include <list>
#include "core.h"
#include "camera.h"
#include "cube.h"
#include "sphere.h"
#include "shape.h"
#include "plane.h"
#include "rope.h"
#include <btBulletDynamicsCommon.h>
#include "BulletSoftBody\btSoftRigidDynamicsWorld.h"
#include "myLeap.h"
#include "myhand.h"
#include "wires.h"
#include "wire.h"

////////////////////////////////////////////////////////////////////////////////

enum Mode
{
	NONE = 0,
	MOVE = 1,
	MOVEBALL = 2,
	DRAW = 3
};

class Display {
public:
	Display();
	~Display();

	void Update();
	void Reset();
	void Draw();
	void Draw2();

	void Quit();

	// Event handlers
	void Resize(int x,int y);
	void Keyboard(int key,int x,int y);
	void KeyboardUp(int key, int x, int y);
	void MouseButton(int btn,int state,int x,int y);
	void MouseMotion(int x,int y);
	void InitializeObjects();
	void ObjectPicking();
	void computeFPS();
	void updateMode();

	// Components
	Camera Cam;
	//objects
	std::list<Shape*> Objects;

private:

	void HandInit();

	void InitializeWorld();
	void AddRigidBody();

	void AddWire(Vector3 from, Vector3 to);

	// Window management
	int WindowHandle;
	int WinX,WinY;

	// Input
	bool LeftDown,MiddleDown,RightDown;
	int MouseX,MouseY;
	Vector3 MouseInWorld;

	//variables
	bool objectClicked = false;
	Shape* clickedObject;
	bool usedDraw2 = false;
	int frameCount = 0;
	int lastFrameTime = 0;

	btDiscreteDynamicsWorld* dynamicsWorld;
	SampleListener leapListener;
	Controller leapController;

	MyHand* LHand;
	MyHand* RHand;
	MyHand* Palm;

	Wires* wires;
	Sphere* sphere;

	Mode mode = NONE;

	GLUquadricObj *obj;

	bool initializeSelect = false;
	Wire* selectedBall = NULL;
};

////////////////////////////////////////////////////////////////////////////////

//Code Source
//https://www.opengl.org/discussion_boards/showthread.php/175823-From-Screen-Coordinate-to-OpenGL-coordinate
static Vector3 GetOGLPos(int x, int y)
{
	GLint viewport[4];
	GLdouble modelview[16];
	GLdouble projection[16];
	GLfloat winX, winY, winZ;
	GLdouble posX, posY, posZ;

	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	glGetDoublev(GL_PROJECTION_MATRIX, projection);
	glGetIntegerv(GL_VIEWPORT, viewport);

	winX = (float)x;
	winY = (float)viewport[3] - (float)y;
	glReadPixels(x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

	gluUnProject(winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);

	return Vector3(posX, posY, posZ);
};

////////////////////////////////////////////////////////////////////////////////

#endif
