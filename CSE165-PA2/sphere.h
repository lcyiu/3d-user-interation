////////////////////////////////////////
// sphere.h
////////////////////////////////////////

#ifndef CSE169_SPHERE_H
#define CSE169_SPHERE_H

#include "core.h"
#include "vector3.h"
#include "matrix34.h"
#include "shape.h"

#define DEFAULTX 0
#define DEFAULTY 5
#define DEFAULTZ 0
#define MASS 10

////////////////////////////////////////////////////////////////////////////////

class Sphere : public Shape
{
public:
	Sphere();
	~Sphere();

	void Update();
	void Reset();
	void Draw();
	void Move(float x, float y, float z);
	string GetShape() { return "sphere"; };

	bool redColor = false;
	bool blueColor = false;
};

////////////////////////////////////////////////////////////////////////////////

#endif
