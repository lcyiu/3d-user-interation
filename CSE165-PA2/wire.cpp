////////////////////////////////////////
// wire.cpp
////////////////////////////////////////

#include "wire.h"
#include <windows.h>		// Header File For Windows
#include <iostream>

////////////////////////////////////////////////////////////////////////////////


//Code Source
//http://lifeofaprogrammergeek.blogspot.com/2008/07/rendering-cylinder-between-two-points.html
void renderCylinder(float x1, float y1, float z1, float x2, float y2, float z2, float radius, int subdivisions, GLUquadricObj *quadric, bool selected, GLfloat model[])
{
	float vx = x2 - x1;
	float vy = y2 - y1;
	float vz = z2 - z1;

	//handle the degenerate case of z1 == z2 with an approximation
	if (vz == 0)
		vz = .0001;

	float v = sqrt(vx*vx + vy*vy + vz*vz);
	float ax = 57.2957795*acos(vz / v);
	if (vz < 0.0)
		ax = -ax;
	float rx = -vy*vz;
	float ry = vx*vz;
	glPushMatrix();

	glColor3f(0.5f, 0.5, 0.5f);
	//draw the cylinder body
	//glTranslatef(x1, y1, z1);
	//glRotatef(ax, rx, ry, 0.0);
	glLoadMatrixf(model);
	//glTranslatef(0, 0, -v);
	gluQuadricOrientation(quadric, GLU_OUTSIDE);
	gluCylinder(quadric, radius, radius, v, subdivisions, 1);

	//draw the first cap
	gluQuadricOrientation(quadric, GLU_INSIDE);
	gluDisk(quadric, 0.0, radius, subdivisions, 1);
	glTranslatef(0, 0, v);

	//draw the second cap
	gluQuadricOrientation(quadric, GLU_OUTSIDE);
	gluDisk(quadric, 0.0, radius, subdivisions, 1);

	if (selected)
	{
		glColor3f(0.0f, 0.0f, 1.0f);
	}
	glutSolidSphere(RADIUS + 0.02 , 10, 10);


	glPopMatrix();
}

void Wire::GetRotationMatrix(float x1, float y1, float z1, float x2, float y2, float z2)
{
	float vx = x2 - x1;
	float vy = y2 - y1;
	float vz = z2 - z1;

	//handle the degenerate case of z1 == z2 with an approximation
	if (vz == 0)
		vz = .0001;

	float v = sqrt(vx*vx + vy*vy + vz*vz);
	float ax = 57.2957795*acos(vz / v);
	if (vz < 0.0)
		ax = -ax;
	float rx = -vy*vz;
	float ry = vx*vz;

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glTranslatef(x1, y1, z1);
	glRotatef(ax, rx, ry, 0.0);
	//glTranslatef(0, 0, v);

	glGetFloatv(GL_MODELVIEW_MATRIX, model);

	glPopMatrix();
}

Wire::Wire() {
	// Initialize constant data
	Size = 1.0f;
	Position = Vector3::ORIGIN;
	Axis = Vector3::YAXIS;

	xLoc = 0;
	yLoc = 0;
	zLoc = 0;

	// Resets variable data
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

Wire::Wire(Vector3 f, Vector3 t, GLUquadricObj* o, float h) {
	// Initialize constant data
	Size = 2.0f;
	Position = Vector3::ORIGIN;
	Axis = Vector3::YAXIS;
	obj = o;
	height = h;
	from = f;
	to = t;
	// Resets variable data

	GetRotationMatrix(from.x, from.y, from.z, to.x, to.y, to.z);

	//collisionShape = new btBoxShape(btVector3(RADIUS, RADIUS, h));
	collisionShape = new btCylinderShapeZ(btVector3(RADIUS*2.0, RADIUS, h/2.0));
	motionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(from.x ,from.y,from.z)));
	btScalar mass = 2;
	btVector3 fallInertia(0, 0, 0);
	collisionShape->calculateLocalInertia(mass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, motionState, collisionShape, fallInertia);
	rigidBody = new btRigidBody(fallRigidBodyCI);
	rigidBody->getWorldTransform().setFromOpenGLMatrix(model);

	Reset();
}

////////////////////////////////////////////////////////////////////////////////

Wire::~Wire() {
	delete collisionShape;
	delete motionState;
	delete rigidBody;
}

////////////////////////////////////////////////////////////////////////////////

void Wire::Update()
{
	//if (picked)
	//{
	//	Move(xLoc, yLoc, zLoc);
	//}
	//else
	//{
	//Update (animate) any variable properties
	btTransform trans;
	rigidBody->getMotionState()->getWorldTransform(trans);

	Vector3 newEnd = to - from;

	from.x = trans.getOrigin().getX();
	from.y = trans.getOrigin().getY();
	from.z = trans.getOrigin().getZ();

	to = from;
	to += newEnd;

	trans.getOpenGLMatrix(model);


	//}
}

////////////////////////////////////////////////////////////////////////////////

void Wire::Reset()
{
}

////////////////////////////////////////////////////////////////////////////////

void Wire::Draw()
{
	renderCylinder(from.x, from.y, from.z, to.x, to.y, to.z, RADIUS, 10, obj, selected, model);
}

////////////////////////////////////////////////////////////////////////////////

void Wire::Move(float x, float y, float z)
{
	lastX = xLoc;
	lastY = yLoc;
	lastZ = zLoc;

	xLoc = x;
	yLoc = y;
	zLoc = z;
}

////////////////////////////////////////////////////////////////////////////////

