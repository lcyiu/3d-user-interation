////////////////////////////////////////
//Code Source
//CSE169 Starter Code
// camera.h
////////////////////////////////////////

#ifndef CSE169_CAMERA_H
#define CSE169_CAMERA_H

#define DEFAULTX 0
#define DEFAULTY -10

#include "core.h"

////////////////////////////////////////////////////////////////////////////////

class Camera {
public:
	Camera();

	void Update();
	void Reset();
	void Draw();

	// Access functions
	void SetAspect(float a)		{Aspect=a;}
	void SetDistance(float d)	{Distance=d;}
	void SetAzimuth(float a)	{Azimuth=a;}
	void SetIncline(float i)	{Incline=i;}
	void StopMoving()			{ moveX = 0; moveY = 0; }

	float GetDistance()			{return Distance;}
	float GetAzimuth()			{return Azimuth;}
	float GetIncline()			{return Incline;}

	void Move(float x, float y, float z) { xloc = x; yloc = y; Distance = z; }

	int moveX, moveY;

private:
	// Perspective controls
	float FOV;		// Field of View Angle
	float Aspect;	// Aspect Ratio
	float NearClip;	// Near clipping plane distance
	float FarClip;	// Far clipping plane distance

	// Polar controls
	float Distance;	// Distance of the camera eye position to the origin
	float Azimuth;	// Rotation of the camera eye position around the Y axis
	float Incline;	// Angle of the camera eye position over the XZ plane
	float xloc, yloc;
};

////////////////////////////////////////////////////////////////////////////////

/*
The Camera class provides a simple means to controlling the 3D camera. It could
be extended to support more interactive controls. Ultimately. the camera sets the
GL projection and viewing matrices.
*/

#endif
