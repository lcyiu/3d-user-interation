/******************************************************************************\
* Copyright (C) 2012-2014 Leap Motion, Inc. All rights reserved.               *
* Leap Motion proprietary and confidential. Not for distribution.              *
* Use subject to the terms of the Leap Motion SDK Agreement available at       *
* https://developer.leapmotion.com/sdk_agreement, or another agreement         *
* between Leap Motion and you, your company or other organization.             *
\******************************************************************************/

#include "myLeap.h"

void SampleListener::onInit(const Controller& controller) {
  std::cout << "Initialized" << std::endl;
}

void SampleListener::onConnect(const Controller& controller) {
  std::cout << "Connected" << std::endl;
  controller.enableGesture(Gesture::TYPE_CIRCLE);
  controller.enableGesture(Gesture::TYPE_KEY_TAP);
  controller.enableGesture(Gesture::TYPE_SCREEN_TAP);
  controller.enableGesture(Gesture::TYPE_SWIPE);
}

void SampleListener::onDisconnect(const Controller& controller) {
  // Note: not dispatched when running in a debugger.
  std::cout << "Disconnected" << std::endl;
}

void SampleListener::onExit(const Controller& controller) {
  std::cout << "Exited" << std::endl;
}

void SampleListener::onFrame(const Controller& controller) {
	// Get the most recent frame and report some basic information
	const Frame frame = controller.frame();
	HandList hands = frame.hands();
	int handCounter = 0;
	bool leftIndex = false;
	bool moveCamera = true;

	for (HandList::const_iterator hl = hands.begin(); hl != hands.end(); ++hl) 
	{
		const Hand hand = *hl;
		std::string handType = hand.isLeft() ? "Left hand" : "Right hand";
		if (handType.compare("Right hand") == 0)
		{
			handCounter++;

			Leap::Vector palmPos = hand.palmPosition();
			float pitch = hand.direction().pitch();
			float yaw = hand.direction().yaw();

			Palm->setPosition(-palmPos.x + 60, -palmPos.y / 10.0, palmPos.z);
			Palm->setAngle(pitch * -90.0 * 1.5, yaw * 180.0);

			// Get index fingers
			const FingerList fingers = hand.fingers();
			for (FingerList::const_iterator fl = fingers.begin(); fl != fingers.end(); ++fl)
			{
				const Finger finger = *fl;
				//My implementation
				if (finger.type() == finger.TYPE_INDEX)
				{
					if (finger.isExtended())
					{
						Leap::Vector lv = finger.tipPosition();
						//cout << lv.x << " " << lv.y << " " << lv.z << endl;
						RHand->setActive(true);
						RHand->setPosition(lv.x / 4.0, lv.y / 10.0, lv.z / 4.0);
					}
					else
					{
						RHand->setActive(false);
						moveCamera = false;
					}
				}
				else if (!finger.isExtended())
				{
					moveCamera = false;
				}
				else
				{
					RHand->setActive(false);
				}
			}
		}
		else
		{
			handCounter++;

			const FingerList fingers = hand.fingers();
			for (FingerList::const_iterator fl = fingers.begin(); fl != fingers.end(); ++fl)
			{
				const Finger finger = *fl;
				if (finger.type() == finger.TYPE_INDEX)
				{
					if (finger.isExtended())
					{
						leftIndex = true;
					}
				}
				else
				{
					if (finger.isExtended())
					{
						moveCamera = false;
					}
				}

			}

			const GestureList gestures = frame.gestures();
			for (int g = 0; g < gestures.count(); ++g) {
				Gesture gesture = gestures[g];

				if (gesture.type() == Gesture::TYPE_CIRCLE) {
					CircleGesture circle = gesture;
					if (circle.state() == Gesture::STATE_STOP) {
						float turns = circle.progress();
						if (turns >= 1)
						{
							LHand->undo = true;
							g = gestures.count();
						}
					}
				}
			}
		}
    }

	if (hands.count() == 2)
	{
		float firsthand = hands[0].palmPosition().x;
		float secondHand = hands[1].palmPosition().x;
		float difference = secondHand - firsthand;
		if (firsthand > secondHand)
		{
			difference = firsthand - secondHand;
		}

		if (difference < 45)
		{
			cout << firsthand << " Clapped " << secondHand << endl;
			Palm->clap = true;
		}
	}

	if (handCounter != 2 || !moveCamera || !leftIndex)
	{
		Palm->setActive(false);
	}
	else if (handCounter == 2 && leftIndex && moveCamera)
	{
		Palm->setActive(true);
	}

	if (leftIndex && RHand->isActive())
	{
		RHand->moveBall = true;
	}
	else
	{
		RHand->moveBall = false;
	}

	if ((Palm->isActive() && handCounter == 2) || RHand->moveBall)
	{
		RHand->setActive(false);
	}
}

void SampleListener::onFocusGained(const Controller& controller) {
  std::cout << "Focus Gained" << std::endl;
}

void SampleListener::onFocusLost(const Controller& controller) {
  std::cout << "Focus Lost" << std::endl;
}

void SampleListener::onDeviceChange(const Controller& controller) {
  std::cout << "Device Changed" << std::endl;
  const DeviceList devices = controller.devices();

  for (int i = 0; i < devices.count(); ++i) {
    std::cout << "id: " << devices[i].toString() << std::endl;
    std::cout << "  isStreaming: " << (devices[i].isStreaming() ? "true" : "false") << std::endl;
  }
}

void SampleListener::onServiceConnect(const Controller& controller) {
  std::cout << "Service Connected" << std::endl;
}

void SampleListener::onServiceDisconnect(const Controller& controller) {
  std::cout << "Service Disconnected" << std::endl;
}
