////////////////////////////////////////
// shape.h
////////////////////////////////////////

#ifndef CSE169_SHAPE_H
#define CSE169_SHAPE_H

#include "core.h"
#include "vector3.h"
#include "matrix34.h"
#include "BulletSoftBody\btSoftBody.h"

#define THRESHOLD 2.0

////////////////////////////////////////////////////////////////////////////////

class Shape {
public:
	virtual void Update() = 0;
	virtual void Reset() = 0;
	virtual void Draw() = 0;
	virtual string GetShape() = 0;
	virtual void Move(float x, float y, float z) = 0;

	btRigidBody* GetRegidBody() { return rigidBody; };
	btSoftBody* GetSoftBody() { return softBody; };

	Vector3 GetLocation() { return Vector3(xLoc, yLoc, zLoc); };
	Vector3 GetLastLocation() { return Vector3(lastX, lastY, lastZ); };

	float xLoc = 9999 , yLoc = 9999, zLoc = 9999;
	float lastX, lastY, lastZ;
	bool picked;
	bool wireNeeded = false;

protected:
	// Constants
	float Size;
	Vector3 Position;
	Vector3 Axis;
	
	// Variables
	float Angle;
	Matrix34 WorldMtx;

	btCollisionShape* collisionShape;
	btDefaultMotionState* motionState;
	btRigidBody* rigidBody;
	btSoftBody* softBody;
};

////////////////////////////////////////////////////////////////////////////////

#endif
