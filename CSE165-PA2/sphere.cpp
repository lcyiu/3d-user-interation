////////////////////////////////////////
// sphere.cpp
////////////////////////////////////////

#include "sphere.h"
#include <windows.h>		// Header File For Windows
#include <iostream>

////////////////////////////////////////////////////////////////////////////////

Sphere::Sphere() {
	// Initialize constant data
	Size = 4.0f;
	Position = Vector3::ORIGIN;
	Axis = Vector3::YAXIS;

	xLoc = DEFAULTX;
	yLoc = DEFAULTY;
	zLoc = DEFAULTZ;

	lastX = DEFAULTX;
	lastY = DEFAULTY;
	lastZ = DEFAULTZ;

	// Resets variable data
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

Sphere::~Sphere() {
	delete collisionShape;
	delete motionState;
	delete rigidBody;
}

////////////////////////////////////////////////////////////////////////////////

void Sphere::Update()
{
	//if (picked)
	//{
	//	Move(xLoc, yLoc, zLoc);
	//}
	//else
	//{
	//	// Update (animate) any variable properties
	//	btTransform trans;
	//	rigidBody->getMotionState()->getWorldTransform(trans);

	//	xLoc = trans.getOrigin().getX();
	//	yLoc = trans.getOrigin().getY();
	//	zLoc = trans.getOrigin().getZ();
	//}
}

////////////////////////////////////////////////////////////////////////////////

void Sphere::Reset()
{
	//xLoc = DEFAULTX;
	//yLoc = DEFAULTY;
	//zLoc = DEFAULTZ;

	//lastX = DEFAULTX;
	//lastY = DEFAULTY;
	//lastZ = DEFAULTZ;

	//Angle = 0.0f;
	//WorldMtx.Identity();
}

////////////////////////////////////////////////////////////////////////////////

void Sphere::Draw()
{
	glPushMatrix();
	glLoadMatrixf(WorldMtx);
	if (redColor)
	{
		glColor3f(1, 0, 0);
	}
	else if (blueColor)
	{
		glColor3f(0.0f, 0.0f, 1.0f);
	}
	else
	{
		glColor3f(0.9f, 0.9f, 0.9f);
	}
	glTranslatef(xLoc, yLoc, zLoc);
	glutSolidSphere(1, 10, 10);
	glLineWidth(1);
	drawAxis(100);
	glPopMatrix();
	
}

////////////////////////////////////////////////////////////////////////////////

void Sphere::Move(float x, float y, float z)
{
	Vector3 cur(x, y, z);
	Vector3 last(lastX, lastY, lastZ);

	if ((cur - last).Mag() > THRESHOLD)
	{
		wireNeeded = true;
	}


	xLoc = x;
	yLoc = y;
	zLoc = z;
}

////////////////////////////////////////////////////////////////////////////////


