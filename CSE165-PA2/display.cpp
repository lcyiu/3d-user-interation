////////////////////////////////////////
// DISPLAY.cpp
////////////////////////////////////////

#include "display.h"

#define WINDOWTITLE	"PA1"

using namespace std;

////////////////////////////////////////////////////////////////////////////////

void lightInit()
{
	// enable lighting
	glEnable(GL_LIGHTING);
	// enable lighting for front
	glLightModeli(GL_FRONT, GL_TRUE);
	// material have diffuse and ambient lighting 
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	// enable color
	glEnable(GL_COLOR_MATERIAL);
	// enable light 0
	glEnable(GL_LIGHT0);

	GLfloat ambientLight[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	GLfloat diffuseLight[] = { 0.5f, 0.5f, 0.5, 1.0f };
	GLfloat specularLight[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat position[] = { 5.0, 5.0, 0.0, 0.0f };

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
	glLightfv(GL_LIGHT0, GL_POSITION, position);
}

////////////////////////////////////////////////////////////////////////////////

void Display::HandInit()
{
	// Have the sample listener receive events from the controller
	LHand = new MyHand();
	RHand = new MyHand();
	Palm = new MyHand();

	leapListener.setObserver(Palm);
	leapListener.setObserver(LHand, RHand);
	leapController.addListener(leapListener);
	leapController.setPolicy(Leap::Controller::POLICY_BACKGROUND_FRAMES);
}

////////////////////////////////////////////////////////////////////////////////

void Display::InitializeWorld()
{
	// Build the broadphase
	btBroadphaseInterface* broadphase = new btDbvtBroadphase();

	// Set up the collision configuration and dispatcher
	btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();
	btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfiguration);

	// The actual physics solver
	btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver;
	 
	// The world.
	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
	dynamicsWorld->setGravity(btVector3(0, -9.81f, 0));
}

////////////////////////////////////////////////////////////////////////////////

void Display::InitializeObjects()
{
	Vector3 v3(-5, 0, 5);
	Shape *plane = new Plane(v3);
	sphere = new Sphere();
	wires = new Wires();
	Objects.push_back(sphere);
	Objects.push_back(plane);
	Objects.push_back(wires);
}

////////////////////////////////////////////////////////////////////////////////

void Display::AddRigidBody()
{
	for (list<Shape*>::const_iterator iterator = Objects.begin(), end = Objects.end(); iterator != end; ++iterator) {
		if ((*iterator)->GetShape().compare("sphere") != 0 && (*iterator)->GetShape().compare("wires") != 0)
		{
			dynamicsWorld->addRigidBody((*iterator)->GetRegidBody());
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

Display::Display() {
	WinX = 640;
	WinY = 480;
	LeftDown = MiddleDown = RightDown = false;
	MouseX = MouseY = 0;

	// Create the window
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(WinX, WinY);
	glutInitWindowPosition(0, 0);
	WindowHandle = glutCreateWindow(WINDOWTITLE);
	glutSetWindowTitle(WINDOWTITLE);
	glutSetWindow(WindowHandle);

	//World Setup
	glClearDepth(1.0f);
	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_CULL_FACE);

	//Light Setup
	lightInit();

	// Background color
	glClearColor(0.0, 0.0, 0.0, 1.0);

	// Initialize components
	Cam.SetAspect(float(WinX) / float(WinY));
	InitializeWorld();
	InitializeObjects();
	AddRigidBody();

	//Initialize Leap ,hand
	HandInit();

	//Initialize cylinder
	obj = gluNewQuadric();
}

////////////////////////////////////////////////////////////////////////////////

Display::~Display() {
	glFinish();
	glutDestroyWindow(WindowHandle);
}

////////////////////////////////////////////////////////////////////////////////

void Display::Update() {
	//update drawing/moving mode
	updateMode();

	//update the dynamics world

	if (Palm->clap)
	{
		dynamicsWorld->stepSimulation(1 / 30.f, 10);
	}
	
	//update Camera
	if (mode == MOVE)
	{
		Cam.Move(Palm->x, Palm->y, Palm->z);
		Cam.SetAzimuth(Palm->yaw);
		Cam.SetIncline(Palm->pitch); 
	}
	Cam.Update();

	// Update the components in the world
	for (list<Shape*>::const_iterator iterator = Objects.begin(), end = Objects.end(); iterator != end; ++iterator) {

		if (Palm->clap)
		{
			(*iterator)->Update();
		}
		
		

		if ((*iterator)->GetShape().compare("sphere") == 0)
		{
			float x = (*iterator)->xLoc;
			float y = (*iterator)->yLoc;
			float z = (*iterator)->zLoc;

			//(*iterator)->Move(x+ 0.01, y + 0.01, z + 0.01);
			if (mode ==  MOVEBALL|| mode == DRAW)
			{
				(*iterator)->Move(RHand->x, RHand->y, RHand->z);

				Vector3 CurrentLoc = (*iterator)->GetLocation();
				Vector3 LastLoc = (*iterator)->GetLastLocation();

				if (sphere->wireNeeded)
				{
					if (mode == DRAW)
					{
						if (selectedBall)
						{
							LastLoc = selectedBall->to;
							selectedBall = NULL;
							initializeSelect = false;
						}

						AddWire(LastLoc, CurrentLoc);
					}
					sphere->lastX = CurrentLoc.x;
					sphere->lastY = CurrentLoc.y;
					sphere->lastZ = CurrentLoc.z;
					sphere->wireNeeded = false;
				}

				if (mode == MOVEBALL)
				{
					selectedBall = wires->SearchWire(CurrentLoc);
					if (selectedBall)
					{
						selectedBall->selected = true;
						sphere->blueColor = true;
					}
					else
					{
						sphere->blueColor = false;
					}
				}
			}
		}
		
	}

	computeFPS();

	// Tell glut to re-display the scene
	glutSetWindow(WindowHandle);
	glutPostRedisplay();
}

////////////////////////////////////////////////////////////////////////////////

void Display::Reset() {
	Cam.Reset();
	Cam.SetAspect(float(WinX) / float(WinY));

	for (list<Shape*>::const_iterator iterator = Objects.begin(), end = Objects.end(); iterator != end; ++iterator) {
		(*iterator)->Reset();
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::Draw() {

	// Begin drawing scene
	glViewport(0, 0, WinX, WinY);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	printLocation(sphere->xLoc, sphere->yLoc, sphere->zLoc, mode);

	for (list<Shape*>::const_iterator iterator = Objects.begin(), end = Objects.end(); iterator != end; ++iterator)
	{
		if ((*iterator)->GetShape().compare("sphere") == 0)
		{
			(*iterator)->Draw();
		}
		else
		{
			(*iterator)->Draw();
		}
	}

	// Finish drawing scene
	glFinish();
	glutSwapBuffers();
}

////////////////////////////////////////////////////////////////////////////////

void Display::Draw2() {
}

////////////////////////////////////////////////////////////////////////////////

void Display::Quit() {
	glFinish();
	glutDestroyWindow(WindowHandle);

	for (list<Shape*>::const_iterator iterator = Objects.begin(), end = Objects.end(); iterator != end; ++iterator) {
		delete (*iterator);
	}

	// Remove the sample listener when done
	leapController.removeListener(leapListener);
	exit(0);
}

////////////////////////////////////////////////////////////////////////////////

void Display::Resize(int x, int y) {
	WinX = x;
	WinY = y;
	Cam.SetAspect(float(WinX) / float(WinY));
}

////////////////////////////////////////////////////////////////////////////////

void Display::Keyboard(int key, int x, int y) {
	switch (key) {
	case 0x1b:		// Escape
		Quit();
		break;
	case 'r':
		Reset();
		break;
	case 'w':
		Cam.moveY = 1;
		break;
	case 's':
		Cam.moveY = -1;
		break;
	case 'a':
		Cam.moveX = 1;
		break;
	case 'd':
		Cam.moveX = -1;
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::KeyboardUp(int key, int x, int y) {
	const float rate = 1.0f;

	switch (key) {
	case 'w':
	case 's':
		Cam.moveY = 0;
		break;
	case 'a':
	case 'd':
		Cam.moveX = 0;
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::MouseButton(int btn, int state, int x, int y) {
	if (btn == GLUT_LEFT_BUTTON) {
		LeftDown = (state == GLUT_DOWN);
	}
	else if (btn == GLUT_MIDDLE_BUTTON) {
		MiddleDown = (state == GLUT_DOWN);
	}
	else if (btn == GLUT_RIGHT_BUTTON) {
		RightDown = (state == GLUT_DOWN);
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::MouseMotion(int nx, int ny) {
	int dx = nx - MouseX;
	int dy = -(ny - MouseY);

	MouseX = nx;
	MouseY = ny;

	// Move camera
	// NOTE: this should really be part of Camera::Update()
	if (RightDown) {
		const float rate = 1.0f;
		Cam.SetAzimuth(Cam.GetAzimuth() + dx*rate);
		Cam.SetIncline(Cam.GetIncline() - dy*rate);
	}
	if (MiddleDown) {
		const float rate = 0.01f;
		Cam.SetDistance(Cam.GetDistance()*(1.0f - dx*rate));
	}
	if (LeftDown)
	{
		if (objectClicked)
		{
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::ObjectPicking() {
}

////////////////////////////////////////////////////////////////////////////////

void Display::computeFPS()
{
	// post: compute frames per second and display in window's title bar
	frameCount++;
	int currentTime = glutGet(GLenum(GLUT_ELAPSED_TIME));
	if (currentTime - lastFrameTime > 1000)
	{
		char s[16];
		sprintf(s, "FPS: %4.2f",
			frameCount * 1000.0 / (currentTime - lastFrameTime));
		glutSetWindowTitle(s);

		lastFrameTime = currentTime;
		frameCount = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::updateMode()
{
	sphere->redColor = false;

	if (Palm->isActive())
	{
		mode = MOVE;
	}
	else
	if (RHand->isActive() && !Palm->isActive())
	{
		mode = DRAW;
		sphere->redColor = true;
		if (selectedBall)
		{
			selectedBall->selected = false;
		}
		Palm->clap = false;
	}
	else
	if (RHand->moveBall)
	{
		mode = MOVEBALL;
		initializeSelect = true;
		if (selectedBall)
		{
			selectedBall->selected = false;
		}
		selectedBall = NULL;
	}
	else if (LHand->undo)
	{
		if (!wires->wires.empty())
		{
			dynamicsWorld->removeConstraint(wires->wires.back()->fixedConstraint);
			dynamicsWorld->removeRigidBody(wires->wires.back()->GetRegidBody());
			//delete wires->wires.back();
			wires->wires.pop_back();
			LHand->undo = false;
		}
	}
	else
	{
		mode = NONE;
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::AddWire(Vector3 from, Vector3 to)
{
	Vector3 direction = to - from;

	////if (!wires->wires.empty())
	////{
	////	Wire* lastWire = wires->wires.back();
	////	Vector3 lastLoc = lastWire->GetLocation();
	////	Vector3 lastDirection = from - lastLoc;
	////	float newHeight = lastDirection.Mag();

	////	float lastAnglexy = atan(lastDirection.y / lastDirection.x) * 180 / PI;
	////	float lastAnglexz = atan(lastDirection.z / lastDirection.x) * 180 / PI;
	////	lastAnglexz = isnan(lastAnglexz) ? 0 : lastAnglexz;
	////	lastAnglexy = isnan(lastAnglexy) ? 0 : lastAnglexy;
	////	lastWire->angleXY = lastAnglexy;
	////	lastWire->angleXZ = lastAnglexz;
	////	lastWire->height = newHeight;
	////}

	//float anglexy = atan(direction.y / direction.x) * 180 / PI;
	//float anglexz = atan(direction.z / direction.x) * 180 / PI;
	//anglexz = isnan(anglexz) ? 0 : anglexz;
	//anglexy = isnan(anglexy) ? 0 : anglexy;

	Wire* wire = new Wire(from, to, obj, (from - to).Mag());

	if (!wires->wires.empty())
	{	
		btTransform trans1;
		btTransform trans2;

		wires->wires.back()->GetRegidBody()->getMotionState()->getWorldTransform(trans1);
		wire->GetRegidBody()->getMotionState()->getWorldTransform(trans2);

		wire->fixedConstraint = new btFixedConstraint(*wires->wires.back()->GetRegidBody(), *wire->GetRegidBody(), trans1, trans2);

		dynamicsWorld->addConstraint(wire->fixedConstraint);
	}
	wires->AddWire(wire);


	dynamicsWorld->addRigidBody(wire->GetRegidBody());
}