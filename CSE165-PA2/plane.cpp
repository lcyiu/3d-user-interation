////////////////////////////////////////
// plane.cpp
////////////////////////////////////////

#include "plane.h"

////////////////////////////////////////////////////////////////////////////////

Plane::Plane(Vector3 tl) {
	// Initialize constant data
	Size = 1.0f;
	Position = Vector3::ORIGIN;
	Axis = Vector3::YAXIS;
	topLeft = tl;
	topRight = tl;
	bottomLeft = tl;
	bottomRight = tl;

	collisionShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
	motionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, -1, 0)));
	btRigidBody::btRigidBodyConstructionInfo
		groundRigidBodyCI(0, motionState, collisionShape, btVector3(0, 0, 0));
	rigidBody = new btRigidBody(groundRigidBodyCI);

	// Resets variable data
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

Plane::~Plane() {
	delete collisionShape;
	delete motionState;
	delete rigidBody;
}

////////////////////////////////////////////////////////////////////////////////

void Plane::Update()
{
	// Update (animate) any variable properties
}

////////////////////////////////////////////////////////////////////////////////

void Plane::Reset()
{
	// Reset dynamic variables to a default state
	Angle = 0.0f;
	WorldMtx.Identity();
}

////////////////////////////////////////////////////////////////////////////////

void Plane::Draw()
{
	glPushMatrix();

	glLoadMatrixf(WorldMtx);
	glColor3f(0.2f, 0.2f, 0.2f);
	glBegin(GL_LINES);
	//y = 0 plane
	for (GLfloat i = -PLANESIZE; i <= PLANESIZE; i += 4) {
		glVertex3f(i, 0, -PLANESIZE); glVertex3f(i, 0, PLANESIZE);
		glVertex3f(PLANESIZE, 0, i); glVertex3f(-PLANESIZE, 0, i);
	}

	//x = -40 plane
	for (GLfloat i = -PLANESIZE; i <= PLANESIZE; i += 4) {
		glVertex3f(-PLANESIZE, i + PLANESIZE, -40); glVertex3f(-PLANESIZE, i + PLANESIZE, 40);
		glVertex3f(-PLANESIZE, 0, i); glVertex3f(-PLANESIZE, 2 * PLANESIZE, i);
	}

	//z = -40 plane
	for (GLfloat i = -PLANESIZE; i <= PLANESIZE; i += 4) {
		glVertex3f(-PLANESIZE, i + PLANESIZE, -PLANESIZE); glVertex3f(PLANESIZE, i + PLANESIZE, -PLANESIZE);
		glVertex3f(-i, 0, -PLANESIZE); glVertex3f(-i, 2 * PLANESIZE, -PLANESIZE);
	}
	glEnd();

	glPopMatrix();
}

////////////////////////////////////////////////////////////////////////////////

void Plane::Move(float x, float y, float z)
{

}

////////////////////////////////////////////////////////////////////////////////


