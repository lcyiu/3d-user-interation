////////////////////////////////////////
// wires.h
////////////////////////////////////////

#ifndef CSE169_WIRES_H
#define CSE169_WIRES_H

#include "core.h"
#include "vector3.h"
#include "matrix34.h"
#include "shape.h"
#include "wire.h"
#include <list>

#define MASS 10

////////////////////////////////////////////////////////////////////////////////

class Wires : public Shape
{
public:
	Wires();
	~Wires();

	void Update();
	void Reset();
	void Draw();
	void Move(float x, float y, float z);
	string GetShape() { return "wires"; };
	Wire* SearchWire(Vector3 v);

	void AddWire(Wire* w);

	list<Wire *> wires;
};

////////////////////////////////////////////////////////////////////////////////

#endif
